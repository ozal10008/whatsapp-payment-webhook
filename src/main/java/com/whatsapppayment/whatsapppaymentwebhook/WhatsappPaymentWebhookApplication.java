package com.whatsapppayment.whatsapppaymentwebhook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WhatsappPaymentWebhookApplication {

	public static void main(String[] args) {
		SpringApplication.run(WhatsappPaymentWebhookApplication.class, args);
	}

}
