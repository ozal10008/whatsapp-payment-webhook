package com.whatsapppayment.whatsapppaymentwebhook.communication;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.codehaus.jettison.json.JSONException;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.codehaus.jettison.json.JSONObject;

@Slf4j
@Component
public class RestClient {
    RestTemplate restTemplate = new RestTemplate();
    public JSONObject sendRequest(String url, JSONObject request,String jwt){
        restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        //headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Content-Type","application/json; charset=UTF-8");
        headers.add("Authorization",jwt);
        HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);
        log.info("url : "+url+" -- request : "+request.toString());
        ResponseEntity<String> result=restTemplate.exchange(url, HttpMethod.POST,entity,new ParameterizedTypeReference<String>() {
        });
        JSONObject js=null;
        try {
            js =new JSONObject(result.getBody());
            log.info("response : "+result.getBody());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return js;
    }
}
