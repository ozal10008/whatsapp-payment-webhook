package com.whatsapppayment.whatsapppaymentwebhook.dto;

import lombok.*;

@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Message {
    private Context context;
    private String from;
    private String id;
    private String timestamp;
    private String type;
    private Button button;
}
