package com.whatsapppayment.whatsapppaymentwebhook.dto;

import lombok.*;

@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Context {
    private String from;
    private String id;
}
