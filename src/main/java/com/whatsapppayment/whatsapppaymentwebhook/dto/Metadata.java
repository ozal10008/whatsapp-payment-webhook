package com.whatsapppayment.whatsapppaymentwebhook.dto;

import lombok.*;

@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Metadata {
    private String display_phone_number;
    private String phone_number_id;
}
