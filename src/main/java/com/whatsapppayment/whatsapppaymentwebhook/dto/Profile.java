package com.whatsapppayment.whatsapppaymentwebhook.dto;

import lombok.*;

@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Profile {
    private String name;
}
