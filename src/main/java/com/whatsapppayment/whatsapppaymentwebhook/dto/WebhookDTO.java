package com.whatsapppayment.whatsapppaymentwebhook.dto;

import lombok.*;

import java.util.List;

@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class WebhookDTO {
    private List<Entry> entry;
    private String object;
}
