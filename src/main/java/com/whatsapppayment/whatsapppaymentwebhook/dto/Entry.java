package com.whatsapppayment.whatsapppaymentwebhook.dto;

import lombok.*;

import java.util.List;

@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Entry {
    private String id;
   private List<Change> changes;
}
