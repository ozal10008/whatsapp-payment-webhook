package com.whatsapppayment.whatsapppaymentwebhook.dto;

import lombok.*;

@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Button {
    private String payload;
    private String text;
}
